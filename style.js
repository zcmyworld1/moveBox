$(function() {
	$("#moveBox").mousedown(function(e) {
		var offset = $(this).offset();
		_x = e.pageX - offset.left; 
		_y = e.pageY - offset.top; 
		$(document).bind("mousemove", function(ev) {
			x = ev.pageX - _x; 
			y = ev.pageY - _y;  
			$("#moveBox").css("left", x);
			$("#moveBox").css("top", y);
		});
	});
	$(document).mouseup(function()  {   
        $(this).unbind("mousemove");
    }) 
})